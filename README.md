# Windows SysTray Utility Program

This program adds a Windows SysTray icon with a menu tu run utilities...

## Release Notes

### 0.0.1

Initial pre-release

## How To Build

```bash
# Clone this repository
$ git clone https://gitlab.com/TKeus/win-sys-utils

# Go into the repository
$ cd win-sys-utils

# Install dependencies
$ dotnet restore
```

- Start VSCode, and run tasks:
  - 'build'
  - 'publish' (creates bin/Release/.../publish)
