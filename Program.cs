using System;
using System.Windows.Forms;

namespace WinSysUtils;

static class Program
{
  /// <summary>
  ///  The main entry point for the application.
  /// </summary>
  [STAThread]
  static void Main()
  {
    // To customize application configuration such as set high DPI settings or default font,
    // see https://aka.ms/applicationconfiguration.
    Application.EnableVisualStyles();
    Application.SetCompatibleTextRenderingDefault(false);

    // Crée l'icône du systray
    NotifyIcon trayIcon = new NotifyIcon();
    trayIcon.Text = "My Systray App";
    trayIcon.Icon = new System.Drawing.Icon("cogs.ico");

    // Crée un menu contextuel pour l'icône du systray
    ContextMenuStrip trayMenu = new ContextMenuStrip();
    trayMenu.Items.Add("Exit", null, OnExit);

    trayIcon.ContextMenuStrip = trayMenu;
    trayIcon.Visible = true;

    // Masquer la fenêtre principale
    Application.Run();
  }

  private static void OnExit(object sender, EventArgs e)
  {
    Application.Exit();
  }
}
